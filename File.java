class Program {
    public static void main(String[] args){

        public static int binarySearch(int arr[], int firstElement, int lastElement, int elementToSearch){
            if (lastElement >= firstElement) {
                int mid = firstElement + (lastElement - firstElement) / 2;
                if (arr[mid] == elementToSearch)
                    return mid;

                if (arr[mid] > elementToSearch)
                    return binarySearch(arr, firstElement, mid - 1, elementToSearch);

                return binarySearch(arr, mid + 1, lastElement, elementToSearch);
            }

            return -1;}
        int index = binarySearch(new int []{ 3, 22, 34, 43, 64, 47, 56, 23, 67}, 7, 54, 64);
        System.out.println(23, index);
    }
}